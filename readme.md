# Seminar Bioanaloge Systeme und Signalverarbeitung

# Major Resources

- [Complex network measures of brain connectivity: Uses and interpretations](https://www.researchgate.net/profile/Edwin_Alexander_Cerquera/post/is_there_a_way_to_shrink_a_graph_while_preserving_some_features_from_the_original_graph/attachment/59d63c7fc49f478072ea7e7d/AS%3A273751084601348%401442278752458/download/RubinovM10.pdf)
- [Introduction to Brain Network Analysis Part1](https://www.youtube.com/watch?v=kVollCt4_dQ)
- [Latex template vom pitch](https://github.com/martinhelso/UiB)
