# Complex network measures of brain connectivity

## Motivation

## Netzwerkerstellung

- fMRT bessere raeumliche zuordnung
- fMRT schlechterer zeitlicher bezug

## Messungen von Gehirnnetzwerken

### 1

- Hirnareale wurden geordnet
- Warme Farben bedeuten hoehe Aehnlichkeit und vice versa
- thresholds noetig, da Subjekte unterschieliche Ausprägungen haben

### 2

- Funktionale Trennung der neuronale Verarbeitung kann durch Cluster erkennt werden (Density)
- Funktionale Integration: Messgrösse der Einfachheit mit der Hirnareale kommunizieren (path lengths)
